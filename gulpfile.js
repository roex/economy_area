'use strict';

/**
 * Dependencies
 */

var	gulp = require('gulp'),
		fileinclude = require('gulp-file-include'),
		watch = require('gulp-watch'),
		prefixer = require('gulp-autoprefixer'),
		uglify = require('gulp-uglify'),
		sourcemaps = require('gulp-sourcemaps'),
		less = require('gulp-less'),
		concat = require('gulp-concat'),
		rigger = require('gulp-rigger'),
		cssmin = require('gulp-minify-css'),
		rimraf = require('rimraf'),
		browserSync = require("browser-sync"),
		globPlugin = require('less-plugin-glob'),
		reload = browserSync.reload;


/**
 * Variables & config definition
 */

var path = {
	build: {
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/img/',
		media: 'build/media/',
		fonts: 'build/fonts/'
	},
	src: {
		html: 'assets/template/pages/*.html',
		js: 'assets/js/main.js',
		vendorjs: 'assets/js/vendor.js',
		style: 'assets/style/main.less',
		img: 'assets/img/**/*.*',
		media: 'assets/media/**/*.*',
		fonts: 'assets/fonts/**/*.*'
	},
	watch: {
		html: 'assets/template/**/*.html',
		js: 'assets/js/**/*.js',
		style: 'assets/style/**/*.less',
		templatestyle: 'assets/template/**/*.less',
		img: 'assets/img/**/*.*',
		media: 'assets/media/**/*.*',
		fonts: 'assets/fonts/**/*.*'
	},
	clean: './build'
};

var config = {
	server: {
		baseDir: "./build"
	},
	tunnel: true,
	host: 'localhost',
	port: 9000,
	logPrefix: "frontend"
};


/**
 * Build section
 */

gulp.task('webserver', function () {
	browserSync(config);
});

gulp.task('clean', function (cb) {
	rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
	gulp.src(path.src.html)
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(gulp.dest(path.build.html))
		.pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
	gulp.src(path.src.js)
		.pipe(rigger())
		.pipe(uglify())
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));
});

gulp.task('vendorjs:build', function () {
	gulp.src(path.src.vendorjs)
		.pipe(rigger())
		.pipe(uglify())
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
	gulp.src(path.src.style)
		.pipe(less({
			plugins: [ globPlugin ]
		}))
		.pipe(concat('main.css'))
		.pipe(prefixer({
			browsers: ['ie >= 7', 'last 25 versions', '> 0.1%'],
			cascade: false
		}))
		.pipe(cssmin())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
	gulp.src(path.src.img)
		.pipe(gulp.dest(path.build.img))
		.pipe(reload({stream: true}));
});

gulp.task('media:build', function() {
	gulp.src(path.src.media)
		.pipe(gulp.dest(path.build.media))
});

gulp.task('fonts:build', function() {
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
	'html:build',
	'js:build',
	'vendorjs:build',
	'style:build',
	'fonts:build',
	'image:build',
	'media:build'
]);


gulp.task('watch', function(){
	watch([path.watch.html], function(event, cb) {
		gulp.start('html:build');
	});
	watch([path.watch.style], function(event, cb) {
		gulp.start('style:build');
	});
	watch([path.watch.templatestyle], function(event, cb) {
		gulp.start('style:build');
	});
	watch([path.watch.js], function(event, cb) {
		gulp.start(['js:build', 'vendorjs:build']);
	});
	watch([path.watch.img], function(event, cb) {
		gulp.start('image:build');
	});
	watch([path.watch.media], function(event, cb) {
		gulp.start('media:build');
	});
	watch([path.watch.fonts], function(event, cb) {
		gulp.start('fonts:build');
	});
});


gulp.task('default', ['build', 'webserver', 'watch']);