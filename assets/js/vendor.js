/**
 * Custom scripts
 */

// Required libraries
//= ../../bower_components/jquery/dist/jquery.min.js


// Bootstrap scripts
//= ../../bower_components/bootstrap/js/transition.js
//= ../../bower_components/bootstrap/js/tab.js
//= ../../bower_components/bootstrap/js/collapse.js



// External libraries
//= ../../bower_components/fancybox/source/jquery.fancybox.pack.js
//= ../../bower_components/select2-dist/dist/js/select2.min.js
//= ../../bower_components/OwlCarousel2/dist/owl.carousel.min.js