/**
 * Custom scripts
 */
$(function () {
    $(document).ready(function () {


        //POP UP
        if ($("a.popup[href='#rent-workplace']").length > 0) {
            var orderForm = $("a.popup[href='#rent-workplace']");
            orderForm.fancybox({
                fitToView: false,
                autoSize: true,
                closeClick: false,
                openEffect: 'fade',
                closeEffect: 'fade',
                closeBtn: true,
                wrapCSS: 'ea-rent-workplace__fancybox'
            });


        }
        //END POP UP


        /* =================================
         ===  select 2                  ====
         =================================== */

        if ($('.ea-select').length > 0) {
            $(".ea-select").select2({
                placeholder: "Страна"
            });
        }


        if ($('.ea-scheme').length > 0) {
            var owlScheme = $('.ea-scheme').owlCarousel({
                margin: 0,
                items: 1,
                dots: false,
                autoHeight: false,
                autoplay: false,
                nav: true,
                //smartSpeed:2500,
                navText: [
                    '<i></i>',
                    '<i></i>'
                ],
                loop: false
            });
        }
    });
});