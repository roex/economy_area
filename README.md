# Front-end для сайта `эконом зона`

Проект разрабатывается на `html` (сетка `bootsrap`) + [less](http://lesscss.org/), иконочный шрифт [Font Awesome](http://fortawesome.github.io/Font-Awesome/), собирается проект на [Gulp](http://gulpjs.com/). Соглашение по именованию css-селекторов [БЭМ](https://ru.bem.info/method/naming-convention/).


### Начало работы
Для сборки проекта нам потребуются `node.js` (https://nodejs.org/), `npm` (https://www.npmjs.com/package/npm) и `bower` (http://bower.io/).
Внешние фреймворки, библиотеки и плагины для верстки (например `jquery` или `bootstrap` размещаются в каталоге `./bower_components` с помощью `bower install ___ --save`) будут устанавливаться автоматически из зависимостей.

#### Установка окружения
1. Если до сих пор не установлен `node.js`, то качаем [тут](https://nodejs.org/), устанавливаем под свою ОС. `npm`, который должен быть в установщике, ставим тоже.
2. С помощью `npm` устанавливаем [bower](http://bower.io/#install-bower) и [gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md):
```bash
$ cd ~
$ npm install -g bower
$ npm install -g gulp
```

#### Установка зависимостей для проекта:

```bash
$ cd ~
$ git clone git@bitbucket.org:
$ npm install
$ bower install
$ gulp
```

Для добавления в зависимости каких-либо плагинов необходимых для разработки (плагины gulp и пр.), необходимо выполнить из корневой директории проекта:
```bash
$ npm install LIBRARY_NAME --save
```

Для добавления в зависимости каких-либо библиотек для вёрстки используем bower, необходимо выполнить из корневой директории проекта:
```bash
$ bower install LIBRARY_NAME --save
```

---

### Терминология

**Блок (blocks)** - самая простая, неделимая сущность.

Пример:
```html
<a href="#" class="ea-button" title="Тайтл ссылки">Ссылка</a>
```

Важно обратить внимание:
**ea-button** - при указании классов для элементов мы придерживаемся БЭМ методологии.

**Компонент (components)** - Состоит из блоков. Также может содержать в себе html-разметку, которую не удалось представить в качестве блоков.

**Страница (page)** - Самая "сложная" сущность фронтенда. Представляет собой всю страницу приложения - полное дерево компонентов и блоков.


## Разработка Front-end


### Соглашение по наименованию

При разработке придерживаемся БЭМ методологии без его шаблонизатора (блок/элемент/модификатор, https://ru.bem.info/method/definitions/) и соглашение по именованию классов (https://ru.bem.info/method/naming-convention/).

#### Структура директорий

Файлы и каталоги для работы над вёрсткой проектов:

```
.
├── assets
│   ├── style
│   ├── fonts
│   ├── img
│   ├── media
│   ├── js
│   │   └── partials
│   └── template
│       ├── blocks
│       ├── components
│       └── pages
├── gulpfile.js
├── package.json
└── build
    ├── css
    ├── fonts
    ├── img
    ├── media
    └── js
```


- **assets** - каталог исходников ресурсов, вся работа (помимо html) ведётся только в данной директирии.
- **assets/style** - файлы стилей (LESS).
- **assets/fonts** - файлы шрифтов (FontAwesome, IcoMoon, ...).
- **assets/img** - файлы изображений (png, jpg, ...).
- **assets/media** - файлы медиа-файлы (видео, flash, ...).
- **assets/js** - файлы скриптов (js, coffee, dart, ...).
- **assets/js/partials** - логически разбитые js-файлы нашего приложения.
- **assets/template** - файлы html-страниц (в корне директории находятся страницы).
- **assets/template/blocks** - файлы повторяющихся элементов или блоков (html).
- **assets/template/components** - файлы компонентов (html).
- **assets/template/pages** - файлы страниц (html).
- **bower.json** - файл модулей и зависимостей Bower.
- **gulpfile.js** - файл с инструкциями сборки (конкатенация файлов, оптимизация изображений, минификация, пре- и пост-процессинг, ...).
- **package.json** - модули и зависимости NPM.
- **build** - Корневой каталог web-сервера (*.html файлы).
- **build/css** - Gulp кладёт сюда обработанные файлы стилей.
- **build/fonts** - Gulp кладёт сюда обработанные файлы шрифтов.
- **build/img** - Gulp кладёт сюда обработанные файлы изображений.
- **build/media** - Gulp кладёт сюда медиа-файлы (видео, flash, ...).
- **build/js** - Gulp кладёт сюда обработанные файлы скриптов.

Каталог `build/*` указан в `.gitignore`, так как результаты сборки не должны присутствовать в репозитории.


#### Файловая структура блоков
```
block-name
        ├── block-name__elem1.less
        ├── block-name__elem1_mod1.less
        ├── block-name__elem1_mod2.less
        ├── block-name__elem2.less
        ├── block-name__elem2_mod1.less
        ├── block-name__elem3.less
        ├── block-name_mod1.less
        ├── block-name.less
        ├── index.html
        └── README.md
```

Правила маппинга подсматривались тут: https://ru.bem.info/tools/bem/bem-tools/levels/#Правила-маппинга-БЭМ-сущностей-в-файловую-систему, но адаптировались под проект

При проектировании и разработке мы используем название в нотации с "тире" (единственный допустимый разделитель): `ea-block-name.`

**Логика именования блоков:**
`ea` - эконом зона
`block-name` - мнемоническое название блока


Папка компонента соответствует его усечённому названию: `./templates/blocks/block-name`.
В корне папки каждого блока находятся как минимум следующие файлы:
`index.html` - содержит разметку компонента.
`block-name.less` - основные стили компонента, в них мы оперируем только классами (БЭМ), файл содержит только правила для класса блока, в идеале никокой вложенности и иерархии, максимум допустимы ::before и ::after:

**Пример block-name.less:**
```less
.ea-block {
    /* ... */
    &::before {
        /* ... */
    }
    &::after {
        /* ... */
    }
}
```

`README.md` - содержит документацию блока (если необходимо). Формат доки утвердим позже.

##### Элементы и модификаторы
###### Модификаторы блока
Располагаются в корне папки блока. Название модификатора соответствует его сути.

Пример файловой структуры блока `ea-button`, у которого есть два модификатора: `ea-button_red` и `ea-button_green`:
```
button
    ├── README.md
    ├── button.less
    ├── button_green.less
    ├── button_red.less
    ├── button__caption.less
    ├── button__caption_bold.less
    └── index.html
```
Содержимое файлов стилей:\
**button.less**
```
.ea-button {
    /* ... */
}
```
**button_green.less**
```
.ea-button_green {
    /* ... */
}
```
**button_red.less**
```
.ea-button_red {
    /* ... */
}
```

**button__caption.less**
```
.ea-button__caption {
    /* ... */
}
```
**button__caption_bold.less**
```
.ea-button__caption_bold {
    /* ... */
}

```
###### Итог
Если собрать всё это воедино, то разметка, генерируемая блоком кнопки, будет выглядеть примерно так (в случае, когда нам нужна красная кнопка с жирной надписью):
```html
<a href="#" class="ea-button ea-button_red">
    <span class="ea-button__caption ea-button__caption_bold"><!-- caption text --></span>
</a>
```